public class Leiloeiro {

    private String nomeLeiloeiro;
    private Leilao leilao;

    public Leiloeiro(String nomeLeiloeiro, Leilao leilao) {
        this.nomeLeiloeiro = nomeLeiloeiro;
        this.leilao = leilao;
    }

    public Leiloeiro() {
    }

    public String getNomeLeiloeiro() {
        return nomeLeiloeiro;
    }

    public void setNomeLeiloeiro(String nomeLeiloeiro) {
        this.nomeLeiloeiro = nomeLeiloeiro;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public Lance retornaMaiorLance(){
        return retornaMaiorLance();


    }
}
