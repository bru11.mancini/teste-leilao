public class Lance {
    public String usuario;
    public double valorLance;

    public Lance(String usuario, double valorLance) {
        this.usuario = usuario;
        this.valorLance = valorLance;
    }

    public Lance() {
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public double getValorLance() {
        return valorLance;
    }

    public void setValorLance(double valorLance) {
        this.valorLance = valorLance;
    }


}
