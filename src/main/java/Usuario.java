public class Usuario {

    private String nomeUsuario;
    private int id;

    public Usuario(String nomeUsuario, int id) {
        this.nomeUsuario = nomeUsuario;
        this.id = id;
    }

    public Usuario() {
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
