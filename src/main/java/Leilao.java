import javax.swing.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Leilao {

    public ArrayList<Lance> leilaoLista = new ArrayList<>();

    public Leilao(ArrayList<Lance> leilaoLista) {
        this.leilaoLista = leilaoLista;
    }

    public Leilao() {
    }

    public ArrayList<Lance> getLeilaoLista() {
        return leilaoLista;
    }

    public void setLeilaoLista(ArrayList<Lance> leilaoLista) {
        this.leilaoLista = leilaoLista;
    }

    public boolean adicionaNovoLance(Lance lanceParametro){
        Lance lance = new Lance();
//        lance.setUsuario("Bruno");
//        lance.setValorLance(123.45);
        //int ultimoIndice = leilaoLista.size();

        if (retornaMaiorLance(leilaoLista) < lance.valorLance){
            leilaoLista.add((leilaoLista.size() + 1), lance);
            return true;
        }
        else {
            return false;
        }
    }

    public double retornaMaiorLance(ArrayList<Lance> leilaoLista){
        int ultimoIndice = (leilaoLista.size() - 1);
        return leilaoLista.get(ultimoIndice).valorLance;
    }
}
